import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;


public class BaseClassTest extends BaseClass {
	
	@Test
	public void test1()
	{
		ExtentHtmlReporter extentreporter = new ExtentHtmlReporter("test.html");		
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(extentreporter);
		ExtentTest test = extent.createTest("Google title page test");
		test.log(Status.INFO, "Launch Google");
		setup();
		String title = driver.getTitle();
		Assert.assertEquals(title, "Google");
		test.log(Status.PASS, "Passed");
		extent.flush();
		
		
	}
	
	@AfterClass
	public void teardown() throws InterruptedException
	{
		Thread.sleep(30000);
		driver.close();
	}
	
	

}
